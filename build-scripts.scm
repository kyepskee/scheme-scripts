(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(use-modules (config))
(use-modules (files))
(use-modules (run))
(use-modules (launcher))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-1))
(use-modules (ice-9 peg))
(use-modules (ice-9 match))

(init-config "build-scripts.sexp")
(define config (config-get-content))

(define-record-type <script>
  (make-script path name)
  script?
  (path script-path)
  (name script-name))

(define (init-scripts)
  (let ((script-list config))
    (map (lambda (script)
           (make-script (make-path (car script))
                        (format #f "~a" (cadr script))))
         script-list)))

(define bash-script-template
  "\
#!/usr/bin/bash

export GUILE_LOAD_PATH={{srcdir}}
guile -e main {{file}} {{args}}
")
(define (script-template srcdir srcfile)
  (define template-main
    `(define (entry args)
       (add-to-load-path ,srcdir)
       (load ,srcfile)
       (main args)))
  (string-append "\
#!/usr/bin/guile \\
-e entry -s
!#
" (format #f "~s" template-main)))


(define-peg-pattern template body (* (and text
                                          (? tag))))
(define-peg-pattern tag all (and (ignore "{{") text (ignore "}}")))
(define-peg-pattern text body (* (and (not-followed-by "{")
                                      (not-followed-by "}")
                                      peg-any)))

(define (get-template text-template tags-alist)
  (define (dotted-to-proper-list lst)
    (if (pair? lst)
        (cons (car lst) (dotted-to-proper-list (cdr lst)))
        (cons lst '())))
  (let* ((match-record (match-pattern template text-template))
         (tree (peg:tree match-record))
         (flattened-tree (concatenate tree))
         (flatlist (dotted-to-proper-list flattened-tree)))
    (string-concatenate
     (map (lambda (elem)
            (match elem
              (('tag tag-sym)
               (cdr (assoc tag-sym tags-alist)))
              (str str)
              (failure "")))
          flatlist))))

(define (acons* key value . rest)
  (cond ((= (length rest) 1)
         (acons key value (car rest)))
        (else
         (acons key value
                (apply acons* rest)))))

(define (script-content path-object src-dir)
  (get-template bash-script-template
                (acons* "file" (path-get-string path-object)
                        "srcdir" (path-get-string src-dir)
                        '())))

(define (compile path-object)
  (run-string-with-args "guild"
                        (list "compile" (path-get-string path-object))))

(define exe-dir (make-path "~/.local/bin/"))


(define (main args)
  (define (get-src-path-object)
    (cond
     ((= (length args) 2)
      (make-path (cadr args)))
     ((not (string-null? (getenv "SCRIPTS_SRC_PATH")))
      (make-path (getenv "SCRIPTS_SRC_PATH")))
     (else
      (error "No source path for scripts provided"))))
  (define (build-script script src-dir)
    (let* ((name-path (make-path (script-name script)))
           (filename-path (script-path script))
           (in-path (append-path src-dir filename-path))
           (out-path (append-path exe-dir name-path)))
      (compile in-path)
      (with-output-to-file (path-get-string out-path)
        (lambda ()
          (display (script-template (path-get-string src-dir)
                                    (path-get-string in-path)))))
      (chmod (path-get-string out-path) #o755)))
  (let ((scripts (init-scripts))
        (src-dir (get-src-path-object)))
    (setenv "GUILE_LOAD_PATH" (path-get-string src-dir))
    (for-each
     (lambda (script)
       (build-script script src-dir))
     scripts)))
