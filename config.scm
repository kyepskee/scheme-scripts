(define-module (config)
  #:export (read-config-file
            init-config
            refresh-config
            config-get-content)
  #:use-module (files)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1))


(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))




;; (define-syntax getconf
;;   (syntax-rules
;;       ((_)
;;        `config-content)
;;       ((_ . fields)
;;        `1)))

;; (define (getconf* fields)
;;   (define (iter fields content)
;;     (if (nil? fields)
;;         content
;;         (assoc))))

(define config-dir (make-path "~/.config/scripts/"))
(define config-path '())
(define config-content '())

;; TODO: add some guards for whether content is read and what to do
;; otherwise
(define (config-get-content)
  config-content)

(define (read-config-file)
  (with-input-from-file (path-get-string config-path)
    (lambda ()
      (set! config-content (read)))))

(define (init-config pathstring)
  (set! config-path (append-path config-dir
                                 (make-path pathstring)))
  (when (not (file-exists? (path-get-string config-path)))
    (touch config-path ""))
  (read-config-file))

(define (refresh-config)
  (read-config-file))
