(define-module (files)
  #:export (file-name-absolute?
            file-name-directory?
            make-path
            path?
            path-get-string
            is-path-absolute?
            is-path-dir?
            append-path
            read-path
            write-path
            ls
            change-filename
            strip-extension
            touch
            spath->path
            append-spath
            mktemp
            rm
            path-exists?
            get-extension
            get-filename)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1))


(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(define (file-name-absolute? path)
  ;; FIXME: what if ~ is escaped
  (or (eq? (string-ref path 0)
           #\~)
      (eq? (string-ref path 0)
           #\/)))
(define (file-name-directory? path)
  (equal? #\/
          (string-ref path
                      (1- (string-length path)))))

(define (new-path dirs abs? dir?)
  (list dirs
        (if abs? 'abs 'rel)
        (if dir? 'dir 'file)))

(define (get-homedir-string)
  (passwd:dir (getpw (getlogin))))

;; TODO: add support for ~
(define* (make-path path #:optional (is-dir? #f))
  (define (loop prev dirs res-dirs)
    (cond
     ((equal? dirs '()) res-dirs)
     ((or (equal? prev "..")
          (equal? (car dirs) ".."))
      (loop (car dirs) (cdr dirs) res-dirs))
     ;; ((equal? (car dirs) "~")
     ;;  (let ((homedir (make-path (get-homedir-string) #t)))
     ;;    (path-dirs
     ;;     (append-path homedir
     ;;                  (new-path (loop '() (cdr dirs) '()))))))
     (else (loop (car dirs) (cdr dirs) (append res-dirs
                                               (list (car dirs)))))))
  (define (replace-homedir path abs?)
    (if (and abs?
             (eq? (string-ref path 0)
                  #\~))
        (string-append (get-homedir-string)
                       (string-drop path 1))
        path))
  (let* ((abs? (file-name-absolute? path))
         (is-dir? (if is-dir? is-dir? (file-name-directory? path)))
         (path (replace-homedir path abs?))
         (dirs (string-split path #\/))
         (dirs (filter (lambda (elem)
                         (not (or (equal? elem "")
                                  (equal? elem "."))))
                       dirs)))
    (new-path (loop '() dirs '())
              abs?
              is-dir?)))

(define (path? path)
  (and (list? path)
       (= (length path) 3)
       (list? (car path))
       (symbol? (cadr path))
       (symbol? (caddr path))))
(define (path-dirs path)
  (car path))
(define (is-path-absolute? path)
  (equal? (cadr path) 'abs))
(define (is-path-dir? path)
  (equal? (caddr path) 'dir))
(define (path-copy path)
  (copy-tree path))


;; get the path object of the appended objects dirpath and fpath
(define (append-path dirpath fpath)
  (when (not (path? dirpath))
    (error "dirpath is not a path object"))
  (when (not (path? fpath))
    (error "fpath is not a path object"))
  (when (not (is-path-dir? dirpath))
    (error (format #f "~s (dirpath) is not a directory" dirpath)))
  (if (is-path-absolute? fpath)
      fpath
      (new-path (append (path-dirs dirpath)
                        (path-dirs fpath))
                (is-path-absolute? dirpath)
                (is-path-dir? fpath))))
(define (dir-of-path path-object)
  (if (is-path-dir? path-object)
      path-object
      (let* ((dirs (path-dirs path-object))
             (len (length dirs)))
        (new-path (take (1- len) dirs)
                  (is-path-absolute? path-object)
                  #t))))
(define (path-get-string path)
  (string-append (if (is-path-absolute? path) "/" "")
                 (string-join (path-dirs path) "/")
                 (if (is-path-dir? path) "/" "")))

(define (path-exists? path-object)
  (file-exists? (path-get-string path-object)))

;; TODO setup contracts and check if this is a path object
(define (read-path path-object)
  (let* ((port (open-input-file (path-get-string path-object)))
         (out (get-string-all port)))
    (close-port port)
    out))
(define (write-path path-object str)
  (let* ((port (open-output-file (path-get-string path-object))))
    (put-string port str)
    (close-port port)))

(define (get-filename path-object)
  (car (last-pair (path-dirs path-object))))

(define (change-filename f path-object)
  (define (list-set lst k val)
    (if (= k 0)
        (cons val (cdr lst))
        (cons (car lst) (list-set (cdr lst)
                                  (1- k)
                                  val))))
  (define new-dirs
    (let ((dirs (path-dirs path-object)))
      (list-set dirs
                (1- (length dirs))
                (f (last dirs)))))
  (new-path new-dirs
            (is-path-absolute? path-object)
            (is-path-dir? path-object)))


(define (get-extension path-object)
  (let ((filename (get-filename path-object)))
    (string-drop filename
                 (string-skip filename
                              (lambda (c) (not (equal? c #\.)))))))

(define (strip-extension path-object)
  (change-filename
   (lambda (filename)
     (string-take filename
                  (string-index filename
                                (lambda (char)
                                  (eq? char #\.)))))
   path-object))

;; (define (strip-extension path-object)
;;   (let ((npath (list-copy path-object)))
;;     (strip-extension! npath)))

;; lists all subfiles and dirs inside a path
(define* (ls path-object #:optional (abs? #f))
  (define (subfile? f)
    (not (or (equal? f ".")
             (equal? f ".."))))
  (map (compose (if abs?
                    (lambda (path)
                      (append-path path-object path))
                    identity)
                make-path)
       (scandir (path-get-string path-object)
                subfile?)))


(define* (touch path-object #:optional content)
  ;; FIXME: check if points to file instead of directory
  (with-output-to-file (path-get-string path-object)
    (lambda ()
      (display content))))

(define (spath->path spath)
  (if (path? spath)
      spath
      (make-path spath)))

(define (append-spath spath1 spath2)
  (append-path (spath->path spath1)
               (spath->path spath2)))

(define (mktemp string-content)
  (let* ((name (string-copy "/tmp/tmp-XXXXXX" 0))
         (port (mkstemp! name)))
    (with-output-to-port port
      (lambda ()
        (display string-content)))
    (close-port port)
    (append-spath "/tmp/" name)))

(define (rm path-object)
  (delete-file (path-get-string path-object)))
