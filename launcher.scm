(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(define-module (launcher)
  #:export (make-option
            ask
            symbol-option
            text-option)
  #:use-module (config)
  #:use-module (files)
  #:use-module (run)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1))


(define (make-option symbol text)
  (cons symbol (string-trim-both text)))
(define (symbol-option option) (car option))
(define (text-option option) (cdr option))

(define (ask prompt options)
  (let* ((lines (string-join (map text-option options)
                             "\n"))
         (len (length options))
         (command "dmenu")
         (args (list "-l" len "-i" "-p" prompt))
         (output (run-string-with-input command args lines))
         (trimmed-output (string-trim-both output))
         (ans-option (find (lambda (e)
                             (equal? (text-option e)
                                     trimmed-output))
                           options)))
    ans-option))
