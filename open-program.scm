(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(use-modules (launcher))
(use-modules (config))
(use-modules (files))
(use-modules (run))
(use-modules (ice-9 match))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-1))

(init-config "open-program.sexp")
(define config
  (config-get-content))
(define-record-type <program>
  (make-program symbol name command)
  program?
  (symbol program-symbol)
  (name program-name)
  (command program-command))

(define (program->option program)
  (make-option (program-symbol program)
               (program-name program)))
(define (init-programs)
  (let ((program-list (cdr (member 'programs config))))
    (map (lambda (program)
           (match program
             ((sym name command)
              (make-program sym name command))))
         program-list)))
(define (run-program program)
  (run-string-daemon (program-command program)))

(define (main args)
  (let* ((programs (init-programs))
         (res (ask "Choose program:"
                   (map program->option programs)))
         (ans-symbol (car res)))
    (when res
      (run-program
       (find (lambda (program)
               (eq? (program-symbol program)
                    ans-symbol))
             programs)))))
