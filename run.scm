(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(define-module (run)
  #:export (make-pipe
            run-string-with-input
            run-string
            run-string-with-args
            run-string-daemon
            daemonize
            fork/pipe)
  #:use-module (files)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 format))

(load-extension (string-append "libguile-" (effective-version))
                "scm_init_popen")

(define (to-string obj)
  (with-output-to-string
    (lambda ()
      (display obj))))

(define (open-pipes mode command args)
  (receive (from to pid)
      (apply open-process mode command args)
    (format #t "test: ~s\n" (input-port? from))
    (values from to)))

(define (make-pipe command args)
  (open-pipes OPEN_BOTH command args))

(define (read-close-pipe pipe)
  (let ((str (get-string-all pipe)))
    (close-pipe pipe)
    str))

(define (read-close-port pipe)
  (let ((str (get-string-all pipe)))
    (close-port pipe)
    str))

(define (make-input-pipe command args)
  (receive (from to pid)
      (apply open-process OPEN_READ command args)
    from))
  ;; (receive (from to)
  ;;     (open-pipes OPEN_READ command
  ;;                 (map to-string args))
  ;;   (display "test")
  ;;   from))

;; (receive (from to) (open-pipes OPEN_READ "date" '("--utc"))
;;   from)

(define (run-string-with-args command-string args)
  (let* ((pipe (make-input-pipe command-string args)))
    (read-close-port pipe)))
  ;; (apply system* command-string args))
;; (define (run-string command-string)
;;   ())

(define (run-string-with-input command-string args input)
  (let ((string-args (map to-string args)))
    (receive (from to) (make-pipe command-string string-args)
      (setvbuf from 'none)
      (setvbuf to 'none)
      (put-string to input)
      (close-port to)
      (read-close-port from))))

(define (child-dissoc)
  (umask 0)
  (setsid)
  (chdir "/"))

(define (daemonize proc)
  (when (zero? (primitive-fork))
    (child-dissoc)
    (proc)))

(define (run-string-daemon command-string)
  (daemonize
   (lambda ()
     (system command-string))))

(define (which path)
  (run-string-with-args "which" (list path)))

;; (define (call-terminally thunk)
;;   (with-continuation
;;    null-continuation
;;    (lambda ()
;;      (with-handler
;;       (lambda (c more)
;;         (display-condition c (current-error-port))
;;         (exit 1))
;;       (lambda ()
;;         (dynamic-wind
;;             (lambda () (values))
;;             thunk
;;             (lambda () (exit 0))))))))

(define (call-terminally thunk)
  (thunk)
  (flush-all-ports)
  (exit 0))

(define (newpipe)
  (let* ((p (pipe))
         (r (car p))
         (w (cdr p)))
    (release-port-handle r)
    (release-port-handle w)
    (cons r w)))

(define* (fork #:optional thunk)
  (flush-all-ports)
  (let ((proc (primitive-fork)))
    (when (and (zero? proc) thunk)
      (call-terminally thunk))
    proc))

(define (run/port+proc thunk)
  (let* ((r/w (pipe))
         (proc (fork (lambda ()
                       (close-input-port (car r/w))
                       (dup2 (port->fdes (cdr r/w)) 1)
                       (thunk)))))
    (close-output-port (cdr r/w))
    (values (car r/w) proc)))

(define* (fork/pipe thunk #:optional conns)
  (let* ((pipes (map (lambda (_) (newpipe)) conns))
         (rev-conns (map reverse conns))
         (froms (map (lambda (conn) (reverse (cdr conn)))
                     rev-conns))
         (conns (if conns conns '((1 0))))
         (tos (map car rev-conns)))
    (let ((proc (primitive-fork)))
      (cond ((= proc -1) (format #t "yoooo wtf\n") (flush-all-ports))
            ((not (zero? proc))                     ; Parent
             (for-each (lambda (to r/w)
                         (let ((w (cdr r/w))
                               (r (car r/w)))
                           (close w)
                           (move->fdes r to)))
                       tos pipes))

            (else                     ; Child
             (for-each (lambda (from r/w)
                         (let ((r (car r/w))
                               (w (cdr r/w)))
                           (close r)
                           (for-each (lambda (fd) (dup2 (port->fdes w) fd)) from)
                           (close w)))
                       froms pipes)
             (call-terminally thunk)))
      proc)))
