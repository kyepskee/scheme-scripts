(let ((f (current-filename)))
  (when f
    (add-to-load-path (dirname f))))

(use-modules (config))
(use-modules (files))
(use-modules (run))
(use-modules (launcher))
(use-modules (srfi srfi-9))
(use-modules (srfi srfi-1))
(use-modules (srfi srfi-11))
(use-modules (ice-9 match))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 regex))
(use-modules (ice-9 ftw))
(use-modules (ice-9 pretty-print))

(define-syntax report
  (syntax-rules ()
    ((report sym)
     (begin
       (format #t "~a: ~s\n" 'sym sym)
       sym))))

(init-config "oi-test.sexp")
(define config
  (config-get-content))
(define oi-path
  (make-path (or (assoc-ref config 'oi-path)
                 "~/work/oi/")))

(define (compare-output out expected)
  (equal? (string-trim-both out)
          (string-trim-both expected)))

(define (run-diff text1 text2)
  (let* ((file1 (mktemp text1))
         (file2 (mktemp text2))
         (out
          (run-string-with-args "diff" `(,(path-get-string file1)
                                         ,(path-get-string file2)))))
    (rm file1)
    (rm file2)
    out))

(define oiejq-fname (path-get-string (make-path "~/work/oi/oiejq/oiejq.sh")))
(define (oiejq-time command-string input)
  (define (get-time error-out)
    (let ((lines (string-split error-out #\newline)))
      (string-drop
       (list-ref lines (- (length lines) 2)) 6)))
  (fork/pipe (lambda ()
               (display input))
             '((1 0)))
  (fork/pipe (lambda ()
               (system (format #f "~a ~a" oiejq-fname command-string)))
             '((1 0) (2 3)))
  (let ((out (get-string-all (fdes->inport 0)))
        (error-out (get-string-all (fdes->inport 3))))
    ;; (report error-out)
    (values out (get-time error-out))))


(define (main args)
  (define (input-path-to-output-name path-object)
    (get-filename
     (if (equal? (get-extension path-object)
                 ".in")
         (change-filename (lambda (filename)
                            (string-append filename ".out"))
                          (strip-extension path-object))
         path-object)))
  (define (s->n s)
    (let ((skipped (string-drop s (string-skip s (lambda (c) (not (char-numeric? c)))))))
      (string->number (string-take skipped (string-skip skipped char-numeric?)))))
  (define (compare-in path1 path2)
    (let* ((fname1 (get-filename path1))
           (fname2 (get-filename path2))
           (n1 (s->n fname1))
           (n2 (s->n fname2)))
      (and (number? n1) (number? n2) (< n1 n2))))
  (define (filter-tests testpath)
    (> (s->n (get-filename testpath)) 61600))
    ;; (not (string-contains (get-filename testpath) "in")))
  (match args
    ((_ exercise . tests)
     (let* ((exercise-path (append-spath oi-path
                                         (string-append exercise "/")))
            (exe-path (append-spath exercise-path exercise))
            (exe-file (path-get-string exe-path))
            (in-path (append-spath exercise-path "in/"))
            (out-path (append-spath exercise-path "out/"))
            (inputs (sort (filter filter-tests (ls in-path #t)) compare-in)))
       ;; TODO: add auto-compilation before-hand
       (for-each (lambda (in-fpath)
                   (let* ((test (get-filename in-fpath))
                          (out-fpath (append-spath out-path
                                                   (input-path-to-output-name in-fpath))))
                     (when (path-exists? out-fpath)
                       (format #t "Running test \"~a\"\n" test)
                       (let*-values (((expected) (read-path out-fpath))
                                     ((out time) (oiejq-time exe-file (read-path in-fpath))))
                         (when (not (compare-output out expected))
                           (format #t "Incorrect output at test ~a!\n" test)
                           (format #t "The diff is:\n~a" (run-diff out expected))
                           (exit))
                         (format #t "Success! Time: ~a/~as\n\n" time 6)))))
                 inputs)))
    (_
     (error "At least the first argument for which exercise it is is needed"))))
